<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('employees', 'EmployeeController@getEmployeesAll');
Route::get('employee/{id}', 'EmployeeController@getEmployee')->where('id', '[0-9]+');
Route::get('getEmployees/{id}', 'EmployeeController@getEmployees');
Route::post('editEmployee/{id}', 'EmployeeController@editEmployee')->where('id', '[0-9]+');
Route::post('addEmployee', 'EmployeeController@addEmployee');
Route::delete('deleteEmployee/{id}', 'EmployeeController@deleteEmployee')->where('id', '[0-9]+');
Route::delete('deleteEmployees/{ids}', 'EmployeeController@deleteEmployees');

