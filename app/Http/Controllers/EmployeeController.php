<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;


class EmployeeController extends Controller
{
    public function getEmployeesAll(){
        $employees = Employee::all();
        return $employees;
    }
    public function getEmployee($id){
        $employee = Employee::find($id);
        return $employee;
    }

    public function getEmployees($id){
        $employees = Employee::findMany(explode(',', $id));
        return $employees;
    }

    public function editEmployee(Request $request, $id){
        $employee = Employee::find($id);
        $employee->surname = $request->surname;
        $employee->name = $request->name;
        $employee->patronymic = $request->patronymic;
        $employee->tel = $request->tel;
        $employee->iin = $request->iin;
        $employee->salary = $request->salary;
        $employee->save();
        return $employee;
    }

    public function addEmployee(Request $request){
        $employee = new Employee();
        $employee->surname = $request->surname;
        $employee->name = $request->name;
        $employee->patronymic = $request->patronymic;
        $employee->tel = $request->tel;
        $employee->iin = $request->iin;
        $employee->salary = $request->salary;
        $employee->save();
        return $employee;
    }

    public function deleteEmployee($id){
        $employee = Employee::find($id);
        if($employee){
            $employee->delete();

        }
        return [ 'success' => true ];;
    }

    public function deleteEmployees($ids){
        Employee::whereIn('id',explode(',', $ids))->delete();
        return [ 'success' => true ];;
    }
}
