import React from 'react';
import ReactDOM from 'react-dom';
import IndexEmployee from '../components/employees/Index'

export default function App() {
    return (
        <IndexEmployee/>
    );
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}