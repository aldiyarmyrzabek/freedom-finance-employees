import * as React from 'react';
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import { visuallyHidden } from '@mui/utils';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormHelperText from "@mui/material/FormHelperText";
import FormControl from "@mui/material/FormControl";
import SearchIcon from '@mui/icons-material/Search';
import MapIcon from '@mui/icons-material/Map';
import AddIcon from '@mui/icons-material/Add';
import NoteAddIcon from '@mui/icons-material/NoteAdd';
import SaveIcon from '@mui/icons-material/Save';
import AddEmployeeDialog from "./add";
import EditEmployeeDialog from "./edit";
import DeleteEmployeeDialog from "./delete";
import DeleteEmployeesDialog from "./delete/multiple.js";

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// This method is created for cross-browser compatibility, if you don't
// need to support IE11, you can use Array.prototype.sort() directly
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: 'fullname',
    numeric: false,
    disablePadding: true,
    label: 'ФИО',
  },
  {
    id: 'iin',
    numeric: true,
    disablePadding: false,
    label: 'ИИН',
  },
  {
    id: 'salary',
    numeric: true,
    disablePadding: false,
    label: 'Зарплата',
  },
  {
    id: 'iban',
    numeric: false,
    disablePadding: false,
    label: 'IBAN',
  },
  {
    id: 'tel',
    numeric: true,
    disablePadding: false,
    label: 'Номер телефона',
  },
  {
    id: 'mailing',
    numeric: false,
    disablePadding: true,
    label: 'Рассылка приглашений',
  },
  {
    id: 'status',
    numeric: false,
    disablePadding: false,
    label: 'Статус',
  },
  {
    id: 'action',
    numeric: true,
    disablePadding: false,
    label: '',
  },
];

function EnhancedTableHead(props) {
  const { onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } =
    props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'Выбрать всех сотрудников',
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};



function EnhancedTableToolbar(props) {

  const numSelected = props.numSelected;
  const selected = props.selected;
  const [openDeleteEmployees, setOpenDeleteEmployees] = React.useState(false);
  const handleDeleteEmployees = (event) => {
    setOpenDeleteEmployees(true);
  }
  const handleCloseDeleteEmployees = () => {
    setOpenDeleteEmployees(false);
    props.refresh()
  };
  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(theme.palette.primary.main, theme.palette.action.activatedOpacity),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: '1 1 100%' }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          Выбрано: {numSelected}
        </Typography>
      ) : (
        <Box >
                <FormControl>
                    <Button variant="outlined" sx={{border: "solid #d1d1d1 1px", color: 'green'}}>
                        Платеж с текущего счета
                    </Button>
                </FormControl>
        </Box>
      )}

      {numSelected > 0 ? (
        <Box >
            <FormControl>
                <Button onClick={handleDeleteEmployees} variant="outlined" sx={{border: "solid #d1d1d1 1px", color: 'green', whiteSpace: 'nowrap', minWidth: 'auto'}}>
                    Удалить выбранных
                </Button>
            </FormControl>
            {openDeleteEmployees && <DeleteEmployeesDialog id={props.selected} open={openDeleteEmployees} refresh={props.refresh} close={handleCloseDeleteEmployees}/>}

        </Box>
      ): ('')}
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
  selected: PropTypes.array,
};

export default function Index() {
  const [order, setOrder] = React.useState('asc');
  const [employees, setEmployees] = React.useState({});
  const [orderBy, setOrderBy] = React.useState('salary');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [refreshDate, setRefreshDate] = React.useState(new Date);
  const [openAddEmployee, setOpenAddEmployee] = React.useState(false);
  const [openEditEmployee, setOpenEditEmployee] = React.useState(false);
  const [employeeId, setEmployeeId] = React.useState(false);
  const [openDeleteEmployee, setOpenDeleteEmployee] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const refreshList = () => {
        setRefreshDate(new Date);
        setSelected([]);
        setEmployeeId(false);
        setOpenEditEmployee(false);
        setOpenAddEmployee(false);
        setOpenDeleteEmployee(false);
    }
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = employees.map((n) => n.id);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleCloseAddEmployee = () => {
    setOpenAddEmployee(false);
    setRefreshDate(new Date())
  };

  const handleCloseEditEmployee = () => {
    setOpenEditEmployee(false);
    setRefreshDate(new Date())
  };

  const handleCloseDeleteEmployee = () => {
    setOpenDeleteEmployee(false);
    setRefreshDate(new Date())
  };

  const handleEditEmployee = (event, id) => {
        setEmployeeId(id)
        setOpenEditEmployee(true);
  }

  const handleDeleteEmployee = (event, id) => {
    setEmployeeId(id)
    setOpenDeleteEmployee(true);
  }

  const isSelected = (id) => selected.indexOf(id) !== -1;


  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - employees.length) : 0;

    React.useEffect(() => {
        getData();
    }, [refreshDate]);

	const getData = () => {
		window.axios.get('/api/employees')
			.then((response) => {
				setEmployees(response.data);
				setIsLoading(false);
			});
	}

    if(isLoading){ return (<div />); }

  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <Box
            component="span"
            m={1}
            display="flex"
            justifyContent="space-between"
            alignItems="center"
        >
            <Box>
                <FormControl sx={{ m: 1 }} variant="outlined">
                    <OutlinedInput
                        sx={{
                            border: "solid #d1d1d1 1px", 
                            "& .MuiInputLabel-root": {color: '#d1d1d1'},//styles the label
                            "& .MuiOutlinedInput-root": {
                            "& > fieldset": { borderColor: "#d1d1d1" },
                            },
                            "& .MuiOutlinedInput-root.Mui-focused": {
                                "& > fieldset": {
                                borderColor: "#d1d1d1"
                                }
                            }
                        }}
                        size='small'
                        id="outlined-adornment-password"
                        startAdornment={
                        <InputAdornment position="start">
                            <IconButton
                            aria-label="search"
                            edge="start"
                            >
                            <SearchIcon />
                            </IconButton>
                        </InputAdornment>
                        }
                    />
                </FormControl>
                <FormControl sx={{ m: 1 }}>
                    <Button variant="outlined" sx={{border: "solid #d1d1d1 1px", color: '#000'}} startIcon={<MapIcon sx={{color: 'green'}} />}>
                        Адрес доставки
                    </Button>
                </FormControl>
            </Box>
            <Box>
                <FormControl sx={{ m: 1 }}>
                    <Button variant="outlined" onClick={() => setOpenAddEmployee(true)} sx={{border: "solid #d1d1d1 1px", color: '#000'}} startIcon={<AddIcon sx={{color: 'green'}} />}>
                        Добавить сотрудника
                    </Button>
                </FormControl>
                <FormControl sx={{ m: 1 }}>
                    <Button variant="outlined" sx={{border: "solid #d1d1d1 1px", color: '#000'}} startIcon={<NoteAddIcon sx={{color: 'green'}} />}>
                        Импорт списка
                    </Button>
                </FormControl>
                <FormControl sx={{ m: 1 }}>
                    <Button variant="outlined" sx={{border: "solid #d1d1d1 1px", color: '#000'}} startIcon={<SaveIcon sx={{color: 'green'}} />}>
                        Экспорт списка
                    </Button>
                </FormControl>
            </Box>
        </Box>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={employees.length}
            />
            <TableBody>
              { employees && 
                stableSort(employees, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((item, index) => {
                    const isItemSelected = isSelected(item.id);
                    const labelId = `enhanced-table-checkbox-${index}`;
                    return (
                        <TableRow
                            hover
                            role="checkbox"
                            aria-checked={isItemSelected}
                            tabIndex={-1}
                            key={item.id}
                            selected={isItemSelected}
                            >
                            <TableCell padding="checkbox">
                                <Checkbox
                                color="primary"
                                checked={isItemSelected}
                                onClick={(event) => handleClick(event, item.id)}
                                inputProps={{
                                    'aria-labelledby': labelId,
                                }}
                                />
                            </TableCell>
                            <TableCell
                                component="th"
                                id={labelId}
                                scope="row"
                                padding="none"
                            >
                                {item.surname + ' ' + item.name + ' ' + item.patronymic}
                            </TableCell>
                            <TableCell align="right">{item.iin}</TableCell>
                            <TableCell align="right">{item.salary}</TableCell>
                            <TableCell align="right">{item.iban}</TableCell>
                            <TableCell align="right">{item.tel}</TableCell>
                            <TableCell align="right">
                                <Button variant="outlined">
                                    Отправить СМС
                                </Button>
                            </TableCell>
                            <TableCell align="right">{item.status}</TableCell>
                            <TableCell align="right">
                                <IconButton disabled={isItemSelected} color="primary" onClick={(e) => handleEditEmployee(e, item.id)} aria-label="Редактировать данные сотрудника" component="label">
                                    <EditIcon/>
                                </IconButton>
                                <IconButton disabled={isItemSelected} onClick={(e) => handleDeleteEmployee(e, item.id)} color="primary" aria-label="Удалить данные сотрудника" component="label">
                                    <DeleteIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    )
                        
                })
              }
              
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={employees.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
          labelRowsPerPage="Показать"
        />
        <EnhancedTableToolbar numSelected = {selected.length} selected={selected} refresh={refreshList}/>
        {openAddEmployee && <AddEmployeeDialog open={openAddEmployee} close={handleCloseAddEmployee} refresh={refreshList}/>}
        {openEditEmployee && <EditEmployeeDialog id={employeeId} open={openEditEmployee} refresh={refreshList} close={handleCloseEditEmployee}/>}
        {openDeleteEmployee && <DeleteEmployeeDialog id={employeeId} open={openDeleteEmployee} refresh={refreshList} close={handleCloseDeleteEmployee}/>}
        
      </Paper>
      {/* <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Dense padding"
      /> */}
    </Box>
  );
}
