import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import TextField from '@mui/material/TextField';
import { MuiTelInput } from 'mui-tel-input'
import PreviousMap from 'postcss/lib/previous-map';

const EditEmployeeDialog = ({open, close, id, refresh}) => {
  const [iin, setIin] = React.useState('');
  const [phone, setPhone] = React.useState('')
  const [surname, setSurname] = React.useState('')
  const [name, setName] = React.useState('')
  const [patronymic, setPatronymic] = React.useState('')
  const [salary, setSalary] = React.useState('')

  
  const NumericOnly = (e) =>{
      const re = /^[0-9\b]+$/;
      if (e.target.value === '' || re.test(e.target.value)) {
         setIin(e.target.value)
      }
  }
  const handleChangePhone = (newPhone) => {
      setPhone(newPhone)
  }

  React.useEffect(() => {
      getData(id);
  }, []);

  const getData = (id) => {
    window.axios.get('/api/employee/'+id)
      .then((response) => {
        setIin(response.data.iin)
        setPhone(response.data.tel)
        setSurname(response.data.surname)
        setName(response.data.name)
        setPatronymic(response.data.patronymic)
        setSalary(response.data.salary)
      });
  }

  const handleEditSubmit = (e) => {
    e.preventDefault()
    const formData = new FormData()

    formData.append('surname', surname)
    formData.append('name', name)
    formData.append('patronymic', patronymic)
    formData.append('iin', iin)
    formData.append('tel', phone)
    formData.append('salary', salary)
    window.axios.post('/api/editEmployee/'+id, formData)
    .then((response) => {
        refresh()
    });
  }

  return (
        <Dialog open={open} onClose={close}>
            <DialogTitle>Редактирование сотрудника</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="iin"
                    label="ИИН"
                    type="string"
                    fullWidth
                    onChange={NumericOnly}
                    variant="outlined"
                    value={iin}
                />
                <TextField
                    margin="dense"
                    id="surname"
                    label="Фамилия"
                    fullWidth
                    variant="outlined"
                    value={surname}
                    onChange={(event)=>{setSurname(event.target.value)}}
                />
                <TextField
                    margin="dense"
                    id="name"
                    label="Имя"
                    fullWidth
                    variant="outlined"
                    value={name}
                    onChange={(event)=>{setName(event.target.value)}}
                />
                <TextField
                    margin="dense"
                    id="patronymic"
                    label="Отчество"
                    fullWidth
                    variant="outlined"
                    value={patronymic}
                    onChange={(event)=>{setPatronymic(event.target.value)}}

                />
                <MuiTelInput 
                    margin="dense" 
                    id="tel" 
                    fullWidth 
                    placeholder="+7 777 123 1234" 
                    disableDropdown 
                    onlyCountries={['KZ', 'RU']} 
                    value={phone} 
                    onChange={handleChangePhone} 
                />
                <TextField
                    margin="dense"
                    id="salary"
                    label="Зарплата"
                    type='number'
                    fullWidth
                    variant="outlined"
                    value={salary}
                    onChange={(event)=>{setSalary(event.target.value)}}

                />
            </DialogContent>
            <DialogActions>
            <Button onClick={close}>Отмена</Button>
            <Button onClick={handleEditSubmit}>Сохранить</Button>
            </DialogActions>
        </Dialog>
  );
}
export default EditEmployeeDialog;
