import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DeleteEmployeeDialog = ({open, close, id, refresh}) => {
  const [surname, setSurname] = React.useState('')
  const [name, setName] = React.useState('')
  const [patronymic, setPatronymic] = React.useState('')

  React.useEffect(() => {
    getData(id);
  }, []);

const getData = (id) => {
  window.axios.get('/api/employee/'+id)
    .then((response) => {
      setSurname(response.data.surname)
      setName(response.data.name)
      setPatronymic(response.data.patronymic)
    });
}

  const handleSubmit = (event) => {
    event.preventDefault()
    window.axios.delete('/api/deleteEmployee/'+id)
    .then((response) => {
        refresh()
    });
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={close}
        TransitionComponent={Transition}
        keepMounted
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Вы уверены, что хотите удалить этих сотрудников?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {surname + ' ' + name + ' ' + patronymic}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={close}>Нет</Button>
          <Button onClick={handleSubmit}>Да</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default DeleteEmployeeDialog
