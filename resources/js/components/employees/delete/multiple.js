import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const DeleteEmployeesDialog = ({open, close, id, refresh}) => {
  const [employees, setEmployees] = React.useState({})

  React.useEffect(() => {
    getData(id);
  }, []);

const getData = (id) => {
  window.axios.get('/api/getEmployees/'+id)
    .then((response) => {
      setEmployees(response.data)
    });
}

  const handleSubmit = (event) => {
    event.preventDefault()
    window.axios.delete('/api/deleteEmployees/'+id)
    .then((response) => {
        close()
        refresh()
    });
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={close}
        TransitionComponent={Transition}
        keepMounted
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle>{"Вы уверены, что хотите удалить этих сотрудников?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <List>
                {employees.length > 0 && employees.map(item => {
                    return (
                        <ListItem>
                            <ListItemText>
                                {item.surname + ' ' + item.name + ' ' + item.patronymic}
                            </ListItemText>
                        </ListItem>
                    )
                    
                })}
            </List>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={close}>Нет</Button>
          <Button onClick={handleSubmit}>Да</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
export default DeleteEmployeesDialog
